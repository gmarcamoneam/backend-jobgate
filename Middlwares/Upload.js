const multer = require("multer");
//le moteur de stockage sur disque vous donne un controle total sur le stockage des fichiers sur le disque

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./Storages");
  },
  filename: (req, file, cb) => {
    console.log(file);

    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  },
});
const filefilter = (req, file, cb) => {
  if (
    file.mimetype == "image/jpeg" ||
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpg"
  ) {
    cb(null, true);
  } else {
    cb(new Error("image upload is not of type jpg/jpeg or png"), false);
  }
};
module.exports = multer({
  storage: storage,
  fileFilter: filefilter,
  limits: { _filesize: 1024 * 1024 * 1024 * 10 },
});
