// 2 eme methode :pour authentification 3al kol

const { Strategy, ExtractJwt } = require("passport-jwt");
const passport = require("passport");
const SECRET = process.env.APP_SECRET;
const User = require("../Models/User");

//pour choisir format token
var options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: SECRET,
};
passport.use(
  new Strategy(options, async ({ id }, done) => {
    // console.log('now in middleware');
    try {
      const user = await User.findById(id);
      if (!user) {
        /*if (not user)*/
        // done(null, "User not found")
        // throw new Error('user not found')
        return done(null, user);
      }
      if (user.role === "admin") {
        return done(null, user);
      } else {
        done(null, false);
      }
    } catch (error) {
      done(null, error.message);
    }
  })
);
