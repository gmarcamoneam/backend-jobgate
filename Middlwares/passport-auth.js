const jwt = require('jsonwebtoken')
const SECRET = process.env.APP_SECRET

passportt = async(req,res,next)=>{
    try {
         // token enregistrer dans l headers
        const token = req.headers['authorization'];
        if(!token){  
            return res.status(404).json({message:'no token'}); 
        }
           //pour verifier date d'expiration de token et recuperer user
        jwt.verify(token,SECRET,(err,decoder)=>{
           // console.log(token);
            if(err){
                return res.status(401).json({status:401, message: 'auth failed' + err});
            }
            //console.log(decoder);
            req.user = decoder;
            next();
        });
    } catch (error) {
        res.status(404).json({message:'auth failed'+ error.message});
    }
};
module.exports = passportt;