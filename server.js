const express = require("express");
const cors = require("cors");
const { success, error } = require("consola");
require("dotenv").config();
//const DB =require.env.APP_DB
const db = require("./Config/database");
const PORT = process.env.APP_PORT || 4000;
const DOMAIN = process.env.APP_DOMAIN;
// const PASS = process.env.APP_PASS
//const PASS = process.env.APP_PASS

const authRoute = require("./Routes/authRoute");
const routespeciality = require("./Routes/RouteSpeciality");
const routecategory = require("./Routes/RouteCategory");
const routeskill = require("./Routes/RouteSkill");
const routecontrat = require("./Routes/RouteContrat");
const routeplace = require("./Routes/RoutePlace");
const routerecommendation = require("./Routes/RouteRecommentation");
const routeoffre = require("./Routes/RouteOffre");
const RouteFavorite = require("./Routes/RouteFavorite");
const routecommentaire = require("./Routes/RouteCommentaire");
const routecandidature = require("./Routes/RouteCandidature");
//const routefavorite = require('./Routes/RouteFavorite')
// les middleware cors express passport
const app = express(); //inilialiser application middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", authRoute);
app.use("/", routespeciality);
app.use("/", routecategory);
app.use("/", routeskill);
app.use("/", routecontrat);
app.use("/", routeplace);
app.use("/", routerecommendation);
app.use("/", routeoffre);
app.use("/", RouteFavorite);
app.use("/", routecommentaire);
app.use("/", routecandidature);

// pour faire afficher image sur postman
app.get("/getfile/:image", function (req, res) {
  res.sendFile(__dirname + "/Storages/" + req.params.image);
});

app.listen(PORT, async () => {
  try {
    success({
      message: `server started on port,${PORT}` + `URL=${DOMAIN}`,
      badge: true,
    });
  } catch (err) {
    error({ message: `error with server`, badge: true });
  }
});
