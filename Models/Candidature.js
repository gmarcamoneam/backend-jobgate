const mongoose = require("mongoose");
const schemCondidature = mongoose.Schema(
  {
    accept: {
      type: Boolean,
      default: false,
    },
    picture: {
      type: String,
      required: false,
    },
    offre: {
      type: mongoose.Types.ObjectId,
      ref: "Offre",
    },
    condidate: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Condidature", schemCondidature);
