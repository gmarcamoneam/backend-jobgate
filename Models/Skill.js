const mongoose = require("mongoose");

const shemaSkill = new mongoose.Schema(
  {
    nbexperience: {
      type: String,
      required: false,
    },
    description: {
      type: String,
      required: false,
    },
    offres: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Offre",
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model("Skill", shemaSkill);
