const mongoose = require("mongoose");

const shemaCommentaire = new mongoose.Schema(
  {
    description: {
      type: String,
    },
    offre: {
      type: mongoose.Types.ObjectId,
      ref: "Offre",
    },
    condidate: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Commentaire", shemaCommentaire);
