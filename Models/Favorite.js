const mongoose = require("mongoose");

const shemafavorite = new mongoose.Schema(
  {
    condidate: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
    offre: {
      type: mongoose.Types.ObjectId,
      ref: "Offre",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Favorite", shemafavorite);
