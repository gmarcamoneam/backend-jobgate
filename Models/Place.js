const mongoose = require("mongoose");

const shemaPlace = new mongoose.Schema(
  {
    name: {
      type: String,
      required: false,
    },
    description: {
      type: String,
      required: false,
    },
    offres: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Offre",
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model("Place", shemaPlace);
