const mongoose = require("mongoose");

const shemaRecommandation = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    listecondidat: {
      //des attributs c1 ,c2
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Recommendation", shemaRecommandation);
