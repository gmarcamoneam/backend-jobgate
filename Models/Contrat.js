const mongoose = require("mongoose");

const shemaContrat = new mongoose.Schema(
  {
    type: {
      type: String,
      required: false,
    },
    offres: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Offre",
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model("Contrat", shemaContrat);
