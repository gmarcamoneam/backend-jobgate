const mongoose = require("mongoose");

const shemaSpeciality = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    categoryies: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Category",
      },
    ],
  },
  { timestamps: true }
);
module.exports = mongoose.model("Speciality", shemaSpeciality);
