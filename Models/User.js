const mongoose = require("mongoose");

const schemaUser = mongoose.Schema(
  {
    fullname: {
      type: String,
      required: false,
      minlength: 2,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    phone: {
      type: Number,
      required: false,
    },
    adresse: {
      type: String,
      required: false,
    },

    picture: {
      type: String,
      required: false,
    },

    cv: {
      type: String,
      required: false,
    },
    git: {
      type: String,
      required: false,
    },
    linkedIn: {
      type: String,
      required: false,
    },

    biography: {
      type: String,
      required: false,
    },
    website: {
      type: String,
      required: false,
    },
    confirmed: {
      type: Boolean,
      default: false,
    },
    verificationcode: {
      type: String,
      required: false,
    },

    resetPasswordToken: {
      type: String,
      required: false,
    },
    resetPasswordExpiresIn: {
      type: Date,
      required: false,
    },
    verificationpassword: {
      type: String,
      required: false,
    },
    verified: {
      type: Boolean,
      required: false,
    },
    role: {
      type: String,
      enum: ["condidate", "admin", "entreprise"],
    },

    candidatures: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
    places: {
      type: mongoose.Types.ObjectId,
      ref: "Place",
    },
    offres: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Offre",
      },
    ],
    specialities: {
      type: mongoose.Types.ObjectId,
      ref: "Speciality",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("User", schemaUser);
