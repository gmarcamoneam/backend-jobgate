const mongoose = require("mongoose");
const shemaOffre = new mongoose.Schema(
  {
    titre: {
      type: String,
      required: false,
    },
    description: {
      type: String,
      required: false,
    },
    dateexpiration: {
      type: String,
      required: false,
    },
    salaire: {
      type: String,
      required: false,
    },
    confirm: {
      type: Boolean,
      default: false,
    },
    skill: {
      type: mongoose.Types.ObjectId,
      ref: "Skill",
    },
    contrat: {
      type: mongoose.Types.ObjectId,
      ref: "Contrat",
    },

    candidatures: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Condidature",
      },
    ],
    commentaires: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Commentaire",
      },
    ],
    favorites: {
      type: mongoose.Types.ObjectId,
      ref: "Favorite",
    },
    place: {
      type: mongoose.Types.ObjectId,
      ref: "Place",
    },
    entreprise: {
      type: mongoose.Types.ObjectId,
      ref: "User",
    },
    category: {
      type: mongoose.Types.ObjectId,
      ref: "Category",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("Offre", shemaOffre);
