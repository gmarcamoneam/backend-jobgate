const { connect } = require('mongoose')
const { success, error } = require('consola')
//require('dotenv').config()
const DB = process.env.APP_DB
const connectDB = async () => {
    try {
        await connect(DB);
        success({
            message: `connect with database \n ${DB}`,
            badge: true
        });
    } catch (err) {
        error({
            message: `unable to connect with database \n ${err}`,
            badge: true
        });
        connectDB();

    };
}

module.exports = connectDB();