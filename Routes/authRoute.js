const route = require("express").Router();
const authController = require("../Controllers/authController");

const Upload = require("../Middlwares/Upload");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

const passportt = require("../Middlwares/passport-auth");

route.post("/registreAdmin", authController.registreAdmin);
route.post("/registre", Upload.single("photo"), authController.registre);

route.post(
  "/registreentreprise",
  Upload.single("photo"),
  authController.registreentreprise
);
route.get("/confirm-now/:verificationcode", authController.verifyEmail);
route.post("/login", authController.login);
route.post("/forgetpassword", authController.forgetpassword);
route.post("/resetpassword/:token", authController.resetpassword);
route.get("/confirmUser/:id", authController.confirmUser);
route.get("/getalluser", authController.getalluser);
route.put(
  "/updateuser/:id" /*,passport.authenticate('jwt',{session:false})*/,
  authController.updateuser
);
route.delete(
  "/deleteuser/:id" /*,passport.authenticate('jwt',{session:false})*/,
  authController.deleteuser
);
route.get("/getAllentreprises", authController.getAllentreprises);
route.get("/getAllcondidate", authController.getAllcondidate);
route.post("/logout", authController.logout);
route.get("/user/:id", authController.getuserById);

route.get("/profile", passportt, authController.profile);
route.delete("/deletecondidate/:id", authController.deletecondidate);
route.delete("/deleteentreprise/:id", authController.deleteentreprise);

module.exports = route;
