const route = require("express").Router();
const Commentaire = require("../Controllers/ControllerCommentaire");

route.post("/createcommentaire", Commentaire.createcommentaire);
route.get("/getAllcommentaires", Commentaire.getAllcommentaires);
route.get("/getcommentaireById/:id", Commentaire.getcommentaireById);
route.get("/getcommentaireByType", Commentaire.getcommentaireByType);
route.put("/updatecommentaire/:id", Commentaire.updatecommentaire);
route.delete("/deletecommentaire/:id", Commentaire.deletecommentaire);

module.exports = route;
