const route = require("express").Router();
const Contrat = require("../Controllers/ControllerContrat");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

route.post(
  "/createcontrat",
  passport.authenticate("jwt", { session: false }),
  Contrat.createcontrat
);
route.get("/getAllContrats", Contrat.getAllContrats);
route.get("/getcontratById/:id", Contrat.getcontratById);
route.get("/getcontratByName", Contrat.getcontratByName);
route.put(
  "/updatecontrat/:id",
  passport.authenticate("jwt", { session: false }),
  Contrat.updatecontrat
);
route.delete(
  "/deletecontrat/:id",
  passport.authenticate("jwt", { session: false }),
  Contrat.deletecontrat
);

module.exports = route;
