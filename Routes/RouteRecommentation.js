const route = require("express").Router();
const Recommendation = require("../Controllers/ControllerRecommendation");

route.post("/createrecommendation", Recommendation.createrecommendation);
route.get("/getAllrecommendations", Recommendation.getAllrecommendations);
route.get("/getrecommendationById/:id", Recommendation.getrecommendationById);
route.get("/getrecommendationByName", Recommendation.getrecommendationByName);
route.put("/updaterecommendation/:id", Recommendation.updaterecommendation);
route.delete("/deleterecommendation/:id", Recommendation.deleterecommendation);

module.exports = route;
