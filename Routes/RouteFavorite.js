const route = require("express").Router();
const favorite = require("../Controllers/ControllerFavorite");

route.post("/createfavorite", favorite.createfavorite);
route.get("/getAllfavorites", favorite.getAllfavorites);
route.get("/getfavoriteById/:id", favorite.getfavoriteById);
route.get("/getfavoriteByName", favorite.getfavoriteByName);
route.put("/updatefavorite/:id", favorite.updatefavorite);
route.delete("/deletefavorite/:id", favorite.deletefavorite);
module.exports = route;
