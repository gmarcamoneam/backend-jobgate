const route = require("express").Router();
const ControllerSkill = require("../Controllers/ControllerSkill");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

route.post(
  "/createskill",
  passport.authenticate("jwt", { session: false }),
  ControllerSkill.createskill
);
route.get("/getAllSkills", ControllerSkill.getAllSkills);
route.get("/getSkillById/:id", ControllerSkill.getSkillById);
route.get("/getSkillByName", ControllerSkill.getSkillByName);
route.put(
  "/updateSkill/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerSkill.updateSkill
);
route.delete(
  "/deleteSkill/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerSkill.deleteSkill
);

module.exports = route;
