const route = require("express").Router();
const ControllerPlace = require("../Controllers/ControllerPlace");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

route.post(
  "/createplace",
  passport.authenticate("jwt", { session: false }),
  ControllerPlace.createplace
);
route.get("/getAllplaces", ControllerPlace.getAllplaces);
route.get("/getplaceById/:id", ControllerPlace.getplaceById);
route.get("/getplaceByName", ControllerPlace.getplaceByName);
route.put(
  "/updateplace/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerPlace.updateplace
);
route.delete(
  "/deleteplace/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerPlace.deleteplace
);

module.exports = route;
