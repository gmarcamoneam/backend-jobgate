const route = require("express").Router();
//const passport = require('passport')
const ControllerCategory = require("../Controllers/ControllerCategory");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

route.post(
  "/createcategory",
  passport.authenticate("jwt", { session: false }),
  ControllerCategory.createcategory
);
route.get("/getAllCategories", ControllerCategory.getAllCategories);
route.get("/getCategoryById/:id", ControllerCategory.getCategoryById);
route.get("/getCategoryByName", ControllerCategory.getCategoryByName);
route.put(
  "/updateCategory/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerCategory.updateCategory
);
route.delete(
  "/deleteCategory/:id",
  passport.authenticate("jwt", { session: false }),
  ControllerCategory.deleteCategory
);
module.exports = route;
