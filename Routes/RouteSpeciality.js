const route = require("express").Router();
const ControllerSpeciality = require("../Controllers/ControllerSpeciality");

route.post("/createSpeciality", ControllerSpeciality.createSpeciality);
route.get("/getAllSpecialities", ControllerSpeciality.getAllSpecialities);
route.get("/getSpecialityById/:id", ControllerSpeciality.getSpecialityById);
route.get("/getSpecialityByName", ControllerSpeciality.getSpecialityByName);
route.put("/updateSpeciality/:id", ControllerSpeciality.updateSpeciality);
route.delete("/deleteSpeciality/:id", ControllerSpeciality.deleteSpeciality);

module.exports = route;
