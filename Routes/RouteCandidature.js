const route = require("express").Router();

const ControllerCandidature = require("../Controllers/ControllerCandidature");

const Uploadcv = require("../Middlwares/Uploadcv");

route.post(
  "/createcandidature",
  Uploadcv.single("photo"),
  ControllerCandidature.createcandidature
);
route.get("/getAllcandidatures", ControllerCandidature.getAllcandidatures);
route.get("/getcandidatureById/:id", ControllerCandidature.getcandidatureById);
route.get("/getcandidatureByName", ControllerCandidature.getcandidatureByName);
route.put("/updatecandidature/:id", ControllerCandidature.updatecandidature);
route.delete("/deletecandidature/:id", ControllerCandidature.deletecandidature);
route.get("/acceptcondidature/:id", ControllerCandidature.acceptcondidature);

module.exports = route;
