const route = require("express").Router();
const ControllerOffre = require("../Controllers/ControllerOffre");
const passport = require("passport");
require("../Middlwares/passport_admin").passport;

route.post("/createoffre", ControllerOffre.createoffre);
route.get("/getAlloffres", ControllerOffre.getAlloffres);
route.get("/getsearchoffre/:key", ControllerOffre.getsearchoffre);
route.get("/getoffreById/:id", ControllerOffre.getoffreById);
route.get("/getoffreByTitre", ControllerOffre.getoffreByTitre);
route.put(
  "/updateoffre/:id",
  /*passport.authenticate('jwt',{session:false}),*/ ControllerOffre.updateoffre
);
route.put(
  "/updateoffres/:id" /*,passport.authenticate('jwt',{session:false})*/,
  ControllerOffre.updateoffres
);

route.delete(
  "/deleteoffre/:id",
  /*passport.authenticate('jwt',{session:false}),*/ ControllerOffre.deleteoffre
);
route.delete(
  "/deleteoffres/:id" /*,passport.authenticate('jwt',{session:false})*/,
  ControllerOffre.deleteoffres
);

route.get("/comfirmoffre/:id", ControllerOffre.comfirmoffre);

module.exports = route;
