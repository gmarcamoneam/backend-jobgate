
const Favorite = require('../Models/Favorite');
const Offre = require('../Models/Offre')
const Condidate = require('../Models/User')


//les function CRUD
createfavorite = async (req,res)=>{
    try {
        const newfavorite = Favorite(req.body)
        await newfavorite.save();
            //relation offre
            await Offre.findByIdAndUpdate({_id:req.body.offre},{$push:{favorites:newfavorite}})
            //relation condidate
            await Condidate.findByIdAndUpdate({_id:req.body.condidate},{$push:{favorites:newfavorite}})
      
        res.status(201).json({
            message: 'favorite created',
            data: newfavorite,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        })
    }
};

getAllfavorites = async (req, res) => {
    try {
        const listfavorites = await Favorite.find({})   
        .populate('offre')
        .populate('condidate');
        res.status(200).json({
            message: 'list favorites',
            data: listfavorites,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
 getfavoriteById = async (req, res) => {
    try {
        const favorite = await Favorite.findById({ _id: req.params.id });
        console.log(favorite)
        res.status(200).json({
            message: 'favorite',
            data: favorite
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    };
}
getfavoriteByName = async (req,res)=>{
    try {
        const favorite = await Favorite.find({name: req.query.name});
        res.status(200).json({
            message: 'favorite',
            data: favorite
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
updatefavorite = async (req,res)=>{
    try {
         await Favorite.updateOne({_id: req.params.id}, req.body);
         res.status(200).json({
            message: 'favorite update',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
} 
deletefavorite = async(req,res)=>{
    try {
        const favorite = await Favorite.findById({_id:req.params.id})
        //relation offre
        await Favorite.findByIdAndUpdate(favorite.offre,{$pull:{favorites:req.params.id}})
        //relation condidate
        await Favorite.findByIdAndUpdate(favorite.condidate,{$pull:{favorites:req.params.id}})

        await Favorite.deleteOne({_id: req.params.id})
        res.status(200).json({
            message: 'favorite deleted',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });  
    }
}
module.exports = { createfavorite, getAllfavorites , getfavoriteById ,getfavoriteByName ,updatefavorite ,deletefavorite};
