const Speciality = require("../Models/Speciality");
const Category = require("../Models/Category");
//les function CRUD
createSpeciality = async (req, res) => {
  try {
    const newSpeciality = Speciality(req.body);
    await newSpeciality.save();
    await Category.findByIdAndUpdate(req.body.categoryies, {
      $push: { specialities: newSpeciality },
    });
    res.status(201).json({
      message: "speciality crée",
      data: newSpeciality,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getAllSpecialities = async (req, res) => {
  try {
    const listSpecialities = await Speciality.find({}).populate("categoryies");
    res.status(200).json({
      message: "list of specialities",
      data: listSpecialities,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getSpecialityById = async (req, res) => {
  try {
    const speciality = await Speciality.findById({
      _id: req.params.id,
    }).populate("categoryies");
    console.log(speciality);
    res.status(200).json({
      message: "speciality",
      data: speciality,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getSpecialityByName = async (req, res) => {
  try {
    const speciality = await Speciality.find({ name: req.query.name }).populate(
      "categoryies"
    );
    res.status(200).json({
      message: "speciality",
      data: speciality,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateSpeciality = async (req, res) => {
  try {
    await Speciality.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "speciality update",
    }); 
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteSpeciality = async (req, res) => {
  try {
    const sp = await Speciality.findById({ _id: req.params.id });
    await Category.findByIdAndUpdate(sp.categoryies, {
      $pull: { Speciality: sp._id },
    });
    await Speciality.deleteOne({ _id: req.params.id });
    res.status(200).json({
      message: "speciality deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};

module.exports = {
  createSpeciality,
  getAllSpecialities,
  getSpecialityById,
  getSpecialityByName,
  updateSpeciality,
  deleteSpeciality,
};
