const Offre = require("../Models/Offre");
const Skill = require("../Models/Skill");
const Place = require("../Models/Place");
const Contrat = require("../Models/Contrat");
const Category = require("../Models/Category");
const Commentaire = require("../Models/Commentaire");
const Candidature = require("../Models/Candidature");
const User = require("../Models/User");

createoffre = async (req, res) => {
  try {
    const newoffre = Offre(req.body);
    await newoffre.save();

    await Skill.findByIdAndUpdate(req.body.skill, {
      $push: { offres: newoffre },
    });
    await Contrat.findByIdAndUpdate(req.body.contrat, {
      $push: { offres: newoffre },
    });
    await Category.findByIdAndUpdate(req.body.category, {
      $push: { offres: newoffre },
    });
    await Place.findByIdAndUpdate(req.body.place, {
      $push: { offres: newoffre },
    });
    await User.findByIdAndUpdate(req.body.entreprise, {
      $push: { offres: newoffre },
    });

    res.status(201).json({
      message: "offre created",
      data: newoffre,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getAlloffres = async (req, res) => {
  try {
    const listoffres = await Offre.find({})
      .populate("entreprise")
      .populate("contrat")
      .populate("skill")
      .populate("commentaires")
      .populate("category")
      .populate("place")
      .populate({ path: "candidatures", populate: { path: "condidate" } });

    res.status(200).json({
      message: "list of offres",
      data: listoffres,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getsearchoffre = async (req, res) => {
  try {
    const offre = await Offre.find({
      $or: [
        { titre: { $regex: req.params.key } },
        { number: { $regex: req.params.key } },
        { date: { $regex: req.params.key } },
        { description: { $regex: req.params.key } },
        { "places.name": { $regex: req.params.key } },
      ],
    });
    res.send(offre);
    res.status(200).json({
      message: "list of recherche",
      data: listoffres,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getoffreById = async (req, res) => {
  try {
    const offre = await Offre.findById({ _id: req.params.id })
      .populate("entreprise")
      .populate("contrat")
      .populate("skill")
      .populate("place")
      .populate("category")
      .populate({ path: "commentaires", populate: { path: "condidate" } })
      .populate({ path: "candidatures", populate: { path: "condidate" } });
    console.log(offre);
    res.status(200).json({
      message: "offre",
      data: offre,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getoffreByTitre = async (req, res) => {
  try {
    const offre = await Offre.find({ titre: req.query.titre })
      .populate("entreprise")
      .populate("contrat")
      .populate("skill")
      .populate("category")
      .populate("place");
    res.status(200).json({
      message: "offre",
      data: offre,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateoffre = async (req, res) => {
  try {
    await Offre.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "offre update",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateoffres = async (req, res) => {
  try {
    await Offre.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "offre update",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteoffre = async (req, res) => {
  try {
    const offre = await Offre.findById({ _id: req.params.id });
    //relation enterprise
    await Offre.findByIdAndUpdate(offre.entreprise, {
      $pull: { offres: req.params.id },
    });
    //relation place
    await Offre.findByIdAndUpdate(offre.place, {
      $pull: { offres: req.params.id },
    });
    //relation skill
    await Offre.findByIdAndUpdate(offre.skill, {
      $pull: { offres: req.params.id },
    });
    //relation contrat
    await Offre.findByIdAndUpdate(offre.contrat, {
      $pull: { offres: req.params.id },
    });
    //relation category
    await Offre.findByIdAndUpdate(offre.category, {
      $pull: { offres: req.params.id },
    });
    //delete
    await Offre.deleteOne({ _id: req.params.id });

    res.status(200).json({
      message: "offre deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteoffres = async (req, res) => {
  try {
    //skill relation
    const off = await Offre.findById({ _id: req.params.id });
    await Offre.findByIdAndUpdate(off.skill, { $pull: { skill: off._id } });
    //place relation
    // const off = await Offre.findById({_id: req.params.id})
    await Offre.findByIdAndUpdate(off.place, { $pull: { place: off._id } });
    //category relation
    await Offre.findByIdAndUpdate(off.category, {
      $pull: { category: off._id },
    });
    //contrat relation
    await Offre.findByIdAndUpdate(off.contrat, { $pull: { contrat: off._id } });
    //entreprise relation
    await Offre.findByIdAndUpdate(off.entreprise, {
      $pull: { entreprise: off._id },
    });
    //commentaire relation
    await Offre.findByIdAndUpdate(off.commentaire, {
      $pull: { commentaires: off._id },
    });
    //condidature relation
    await Offre.findByIdAndUpdate(off.candidature, {
      $pull: { candidatures: off._id },
    });

    await Offre.deleteOne({ _id: req.params.id });
    res.status(200).json({
      message: "offre deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
comfirmoffre = async (req, res) => {
  try {
    const offre = await Offre.findById({ _id: req.params.id });
    offre.confirm = true;
    offre.save();
    res.status(200).json({
      message: "confirm true",
      data: offre,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
module.exports = {
  createoffre,
  getAlloffres,
  getsearchoffre,
  getoffreById,
  getoffreByTitre,
  updateoffre,
  updateoffres,
  deleteoffre,
  deleteoffres,
  comfirmoffre,
};
