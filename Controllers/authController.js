const User = require("../Models/User");
const Commentaire = require("../Models/Commentaire")
const Candidature = require("../Models/Candidature")
const Offre = require("../Models/Offre")

const bcrypt = require("bcrypt");


//importation de model jsonwebtoken
const jwt = require("jsonwebtoken");

const { randomBytes } = require("crypto");
const { join } = require("path");
//
const nodemailer = require("nodemailer");
var transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "03aade3ded14ab",
    pass: "fb1c1dbaecd53f",
  },
});
var RefreshTokens = [];
var tokenList=[]
const DOMAIN = process.env.APP_DOMAIN;
const SECRET = process.env.APP_SECRET;


registreAdmin = async (req, res) => {
  try {
    const password = bcrypt.hashSync(req.body.password, 10);
    const admin = new User({
      ...req.body,
      password,
      confirmed: true,
      role: "admin",
    });
    await admin.save();
    res.status(201).json({
      status: 201, data:admin,
      message: "Hurray your account admin is created",
    });
  } catch (error) {
    res.status(406).json({
      status: 406,
      message: error.message,
    });
  }
};

registre = async (req, res) => {
  try {
    const password = bcrypt.hashSync(req.body.password, 10);
      req.body["picture"] = req.file.filename;
    const user = new User({
      ...req.body,
      password,
      role: "condidate",
      confirmed: false,
      verificationcode: randomBytes(6).toString("hex"),
    });
       await user.save();

    res
      .status(201)
      .json({ data:user, message: "Your account is created verify your email address" });
    transporter.sendMail(
      {
        to: user.email,
        subject: "welcome" + " " + user.fullname,
        text: "Bonjour Mr/Mme",
        html: `
                          <h2>Hello ${user.fullname}</h2>
                          <p>we are glad to have you on board at  ${user.fullname}. </p>
                          <a href="${DOMAIN}confirm-now/${user.verificationcode}">confirm now</a>
                   `,
      },  
      (err, info) => {
        if (err) {
          console.log("error:" + err.message);
        } else {
          console.log("Email sent: ", info.response);
        }
      }
    );
  } catch (error) {
    res.status(406).json({ message: "error is " + error.message });
  }
};


registreentreprise = async (req, res) => {
  try {
    req.body["picture"] = req.file.filename;
    const password = bcrypt.hashSync(req.body.password, 10);
    const entreprise = new User({
      // l user kanet bl u walet bl U
      ...req.body,
      password,
      confirmed: false,
      role: "entreprise",
    });
    await entreprise.save();
    res.status(201).json({
      status: 201, data:entreprise, 
      message: "Hurray your account entreprise is created",
    });
  } catch (error) {
    res.status(406).json({
      status: 406,
      message: error.message,
    });
  }
};


verifyEmail = async (req, res) => {
  try {
    const user = await User.findOne({
      verificationcode: req.params.verificationcode,
    });

    user.confirmed = true;
    user.verificationcode = undefined;
    user.save();
    //console.log(user)
    res.sendfile(join(__dirname, "../templates/verification_success.html"));
  } catch (error) {
    res.sendfile(join(__dirname, "../templates/errors.html"));
  }
};
login = async (req, res) => {
  try {
    //1 ere etape verifier email et compare password
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({
        status: 404,
        message: "email  not found !",
      });
    }
    if (user.confirmed === true) {
      const passwordCompare = bcrypt.compareSync(password, user.password);

      if (!passwordCompare) {
        return res.status(404).json({
          status: 404,
          message: "password incorrect",
        });
      }
      // 2 eme etape creation de token
      const token = jwt.sign(
        {
          id: user._id,
          user: user,
        },
        SECRET,
        {
          expiresIn: "1 days",
        }
        /* {expiresIn: '24h'} */
      );
      const refreshToken = jwt.sign({ id: user._id }, SECRET, {
        expiresIn: "86400",
      });
      RefreshTokens[refreshToken] = user._id;
      const result = {
        email: user.email,
        user: user,
        token: token,
        expiresIn: 1,
        refreshToken
        //key : value
      };
      res.status(200).json({
        ...result,
        message: "hurray! you are now loged in.",
        success: true,
      });
    } else {
      return res.status(404).json({
        message: "you are not confirmed",
        success: false,
      });
    }
  } catch (error) {
    res.status(404).json({
      status: 404,
      message: error.message,
    });
  }
};



 forgetpassword = async (req, res) => {
  try {
    const email = req.body.email;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(404).json({ message: "user email not found" });
    }
    const token = jwt.sign(
      {
        id: user._id,
        user: user,
      },
      SECRET,
      {
        expiresIn: "24h",
      }
    ); 
    await User.findOneAndUpdate(
      { email: email },
      { resetPasswordToken: token }
    );
    res.status(200).json({ token: token, message: "token created verifier your email" });
   transporter.sendMail(
      {
        subject: "forget password",
        text: "bonjour moneam",
        to: user.email,
        html: `
    <a href="http://localhost:4200/resetpassword/${token}">reset password</a>

`,
      },
         
    );
  } catch (error) {
    res.status(404).json({
      message: "error is" + error.message,
    });
  }
};  




 resetpassword = async (req, res) => {
  try {
    const resetPasswordToken = req.params.token;
    if (resetPasswordToken) {
      // pour verifier date d'expiration de resetPasswordtoken
      jwt.verify(resetPasswordToken, SECRET, async (err) => {
        if (err) {
          return res.json({ error: "incorrect token or it is expired " });
        } 
        const user = await User.findOne({
          resetPasswordToken: resetPasswordToken,
        });

        user.password = bcrypt.hashSync(req.body.newPass, 10);
        user.resetPasswordToken = undefined;
        user.save();
        return res.status(200).json({
          message: "password changée",
        });
      });
    }
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
}; 
confirmUser = async (req, res) => {
  try {
    const user = await User.findById({ _id: req.params.id });
    user.confirmed = true;
    user.save();
    res.status(200).json({
      message: "confirm true",
      data: user
    });
  } catch (error) {
    res.status(406).json({
      status: 406,
      message: error.message,
    });
  }
};



getalluser = async (req, res) => {
  try {
    const users = await User.find({});
    res.status(201).json({
      message: "list users",
      data: users,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateuser = async (req, res) => {
  try {
    const users = await User.findByIdAndUpdate({ _id: req.params.id }, req.body,{new:true});
    res.status(201).json({
      message: "profil update",
      data:users
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};

deleteuser = async (req, res) => {
  try {
    const users = await User.deleteOne({ _id: req.params.id })//.find({ role: "entreprise"});
    res.status(200).json({
      message: "user deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};




getAllentreprises = async (req, res) => {
  try {
    const entreprise = await User.find({ role: "entreprise"})
    .populate('offres')
    res.status(200).json({
      msg: 'list entreprises',
      data: entreprise, //await Company.find({}),
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
}
getAllcondidate = async (req, res) => {
  try {
    const condidate = await User.find({ role: "condidate"})
    res.status(200).json({
      msg: 'list condidates',
      data: condidate, //await Company.find({}),
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
}
  logout = async(req,res)=>{
    try {
      console.log("tokenList:", tokenList)
      var refreshtoken = req.body.refreshToken
      if(refreshtoken in tokenList){
        delete tokenList[refreshtoken]
      }
      console.log("tokenlist", tokenList)
      res.status(200).json({
        message:'logout avec success',
        data: tokenList
      })
    } catch (error) {
      res.status(400).json({
        message: error.message, //message:"logout account", data: tokenList
      });
    }
  }

  profile = async (req, res) => {
    try {
        const user = req.user;
        res.status(200).json({ user: user });
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
};


 getuserById = async (req, res) => {
  try {
      const user = await User.findById({ _id: req.params.id })
       .populate({path:'offres',populate:{path:"place"}})
       .populate({path:'offres',populate:{path:"skill"}})
       .populate({path:'offres',populate:{path:"contrat"}})
       .populate({path:'offres',populate:{path:"category"}})
      res.status(200).json({
          message: 'list offre by',
          data: user
      });
  } catch (error) {
      res.status(400).json({
          message: error.message,
      });
  };
}
deletecondidate = async (req, res) => {
  try {
 
    await Commentaire.deleteMany({candidate:req.params.id})
    await Candidature.deleteMany({candidate:req.params.id})

    const users = await User.deleteOne({ _id: req.params.id })//.find({ role: "entreprise"});
    res.status(200).json({
      message: "user deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteentreprise = async (req, res) => {
  try {
    await Offre.deleteMany({entreprise:req.params.id})
    const users = await User.deleteOne({ _id: req.params.id })//.find({ role: "entreprise"});
    res.status(200).json({
      message: "user deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};




























module.exports = {
  registreAdmin,
  registre,
  registreentreprise,
  verifyEmail,
  login,
  forgetpassword,
  resetpassword,
  confirmUser,
  getalluser,
  updateuser,
  deleteuser,
  getAllentreprises,
  getAllcondidate,
  logout,
  getuserById,
  profile,
  deletecondidate,deleteentreprise
};
