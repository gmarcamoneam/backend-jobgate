const Place = require("../Models/Place");
const Recommandation = require("../Models/Recommandation");

//les function CRUD
createrecommendation = async (req, res) => {
  try {
    const newrecommendation = Recommandation(req.body);
    await newrecommendation.save();
    res.status(201).json({
      message: "recommendation crée",
      data: newrecommendation,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getAllrecommendations = async (req, res) => {
  try {
    const listrecommendations = await Recommandation.find({});
    res.status(200).json({
      message: "list of recommendations",
      data: listrecommendations,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getrecommendationById = async (req, res) => {
  try {
    const recommendation = await Recommandation.findById({
      _id: req.params.id,
    });
    console.log(recommendation);
    res.status(200).json({
      message: "recommendation",
      data: recommendation,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getrecommendationByName = async (req, res) => {
  try {
    const recommendation = await Recommandation.find({ name: req.query.name });
    res.status(200).json({
      message: "recommendation",
      data: recommendation,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updaterecommendation = async (req, res) => {
  try {
    await Recommandation.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "recommendation update",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleterecommendation = async (req, res) => {
  try {
    await Recommandation.deleteOne({ _id: req.params.id });
    res.status(200).json({
      message: "recommendation deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
module.exports = {
  createrecommendation,
  getAllrecommendations,
  getrecommendationById,
  getrecommendationByName,
  updaterecommendation,
  deleterecommendation,
};
