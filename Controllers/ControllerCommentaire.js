const Commentaire = require('../Models/Commentaire')

const offre = require('../Models/Offre')
const condidate = require('../Models/User')


//les function CRUD
createcommentaire = async (req,res)=>{
    try {
        const newcommentaire = Commentaire(req.body)
        await newcommentaire.save();
          //relation offre
            await offre.findByIdAndUpdate(req.body.offre,{$push:{commentaires:newcommentaire}})
          //relation condidate
            await condidate.findByIdAndUpdate(req.body.condidate,{$push:{commentaires:newcommentaire}})  
        res.status(201).json({
            message: 'commentaire crée',
            data: newcommentaire,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        })
    }  
};
getAllcommentaires = async (req, res) => {
    try {
        const listcommentaires = await Commentaire.find({})
        .populate('offre')
        .populate('condidate');
        ;
        res.status(200).json({
            message: 'list of commentaires',
            data: listcommentaires,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
getcommentaireById = async (req, res) => {
    try {
        const commentaire = await Commentaire.findById({ _id: req.params.id })
        .populate('offre')
         .populate('condidate');
        console.log(commentaire)
        res.status(200).json({
            message: 'commentaire',
            data: commentaire
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    };
}
getcommentaireByType = async (req,res)=>{
    try {
        const commentaire = await Commentaire.find({type: req.query.type});
        res.status(200).json({
            message: 'commentaire',
            data: commentaire 
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
updatecommentaire = async (req,res)=>{
    try {
         await Commentaire.updateOne({_id: req.params.id}, req.body);
         res.status(200).json({
            message: 'commentaire update',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
deletecommentaire = async(req,res)=>{
    try {
      const commemtaire=  await Commentaire.deleteOne({_id: req.params.id})
          //relation offre
          await Candidature.findByIdAndUpdate(commemtaire.offre,{$pull:{commentaires:req.params.id}})
          //relation condidate
          await Candidature.findByIdAndUpdate(commemtaire.condidate,{$pull:{commentaires:req.params.id}})
        res.status(200).json({
            message: 'commentaire deleted',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });  
    }
}
module.exports = { createcommentaire, getAllcommentaires, getcommentaireById ,getcommentaireByType ,updatecommentaire ,deletecommentaire};