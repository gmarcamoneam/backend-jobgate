const Place = require("../Models/Place");

//les function CRUD
createplace = async (req, res) => {
  try {
    const newplace = Place(req.body);
    await newplace.save();
    res.status(201).json({
      message: "place crée",
      data: newplace,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getAllplaces = async (req, res) => {
  try {
    const listplaces = await Place.find({});
    res.status(200).json({
      message: "list of places",
      data: listplaces,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getplaceById = async (req, res) => {
  try {
    const place = await Place.findById({ _id: req.params.id });
    console.log(place);
    res.status(200).json({
      message: "place",
      data: place,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getplaceByName = async (req, res) => {
  try {
    const place = await Place.find({ name: req.query.name });
    res.status(200).json({
      message: "place",
      data: place,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateplace = async (req, res) => {
  try {
    await Place.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "place update",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteplace = async (req, res) => {
  try {
    await Place.deleteOne({ _id: req.params.id });
    res.status(200).json({
      message: "place deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
module.exports = {
  createplace,
  getAllplaces,
  getplaceById,
  getplaceByName,
  updateplace,
  deleteplace,
};
