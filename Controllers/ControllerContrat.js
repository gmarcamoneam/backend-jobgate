
const Contrat = require('../Models/Contrat')

//les function CRUD
createcontrat = async (req,res)=>{
    try {
        const newcontrat = Contrat(req.body)
        await newcontrat.save();
        res.status(201).json({
            message: 'contrat crée',
            data: newcontrat,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        })
    }
};
getAllContrats = async (req, res) => {
    try {
        const listcontrats = await Contrat.find({});
        res.status(200).json({
            message: 'list of contrats',
            data: listcontrats,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
getcontratById = async (req, res) => {
    try {
        const contrat = await Contrat.findById({ _id: req.params.id });
        console.log(contrat)
        res.status(200).json({
            message: 'contrat',
            data: contrat
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    };
}
getcontratByName = async (req,res)=>{
    try {
        const contrat = await Contrat.find({type: req.query.type});
        res.status(200).json({
            message: 'contrat',
            data: contrat
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
updatecontrat = async (req,res)=>{
    try {
         await Contrat.updateOne({_id: req.params.id}, req.body);
         res.status(200).json({
            message: 'contrat update',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
deletecontrat = async(req,res)=>{
    try {
        await Contrat.deleteOne({_id: req.params.id})
        res.status(200).json({
            message: 'contrat deleted',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });  
    }
}
module.exports = { createcontrat, getAllContrats, getcontratById ,getcontratByName ,updatecontrat ,deletecontrat};
