const Candidature = require('../Models/Candidature');
const Offre = require('../Models/Offre')
const Condidate = require('../Models/User')

//les function CRUD
createcandidature = async (req,res)=>{
    try {
      req.body["picture"] = req.file.filename;

        const newcandidature = Candidature(req.body)
        await newcandidature.save();
        //relation offre
        await Offre.findByIdAndUpdate({_id:req.body.offre},{$push:{candidatures:newcandidature}})
        //relation condidate
        await Condidate.findByIdAndUpdate({_id:req.body.condidate},{$push:{candidatures:newcandidature}})

        res.status(201).json({
            message: 'candidature crée',
            data: newcandidature,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        })
    }
};
getAllcandidatures = async (req, res) => {
    try {
        const listcandidatures = await Candidature.find({})
        .populate({path:'offre',populate:{path:'place'}})
        .populate({path:'offre',populate:{path:'entreprise'}})
        .populate({path:'offre',populate:{path:'contrat'}})
        .populate({path:'condidate',populate:{path:'candidatures'}})

       // .populate('offre')
        // .populate('condidate');
        res.status(200).json({
            message: 'list of candidatures',
            data: listcandidatures,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
getcandidatureById = async (req, res) => {
    try {
        const candidature = await Candidature.findById({ _id: req.params.id })
         .populate('offre')
         .populate('condidate');
        console.log(candidature)
        res.status(200).json({
            message: 'candidature',
            data: candidature
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    };
}
getcandidatureByName = async (req,res)=>{
    try {
        const candidature = await Candidature.find({name: req.query.name})
        .populate('offre')
        .populate('condidate');
        res.status(200).json({
            message: 'candidature',
            data: candidature
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
updatecandidature = async (req,res)=>{
    try {
         await Candidature.updateOne({_id: req.params.id}, req.body);
         res.status(200).json({
            message: 'candidature update',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
deletecandidature = async(req,res)=>{
    try {
        const condidature = await Candidature.findById({_id:req.params.id})
        //relation offre
        await Candidature.findByIdAndUpdate(condidature.offre,{$pull:{candidatures:req.params.id}})
        //relation condidate
        await Candidature.findByIdAndUpdate(condidature.condidate,{$pull:{candidatures:req.params.id}})

        await Candidature.deleteOne({_id: req.params.id})
        res.status(200).json({
            message: 'candidature deleted',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });  
    }
}
acceptcondidature = async(req,res)=>{
    try {
       const condidature=  await Candidature.findById({_id:req.params.id})
        condidature.accept=true
        condidature.save()
        res.status(200).json({
            message: 'accept avec succes',
            data:condidature
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
};
module.exports = { createcandidature, getAllcandidatures, getcandidatureById ,getcandidatureByName ,updatecandidature ,deletecandidature,acceptcondidature};





