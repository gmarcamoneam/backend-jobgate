const Skill = require("../Models/Skill");

//les function CRUD
createskill = async (req, res) => {
  try {
    const newSkill = Skill(req.body);
    await newSkill.save();
    res.status(201).json({
      message: "Skill crée",
      data: newSkill,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getAllSkills = async (req, res) => {
  try {
    const listSkills = await Skill.find({});
    res.status(200).json({
      message: "list of Skills",
      data: listSkills,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getSkillById = async (req, res) => {
  try {
    const skill = await Skill.findById({ _id: req.params.id });
    console.log(skill);
    res.status(200).json({
      message: "skill",
      data: skill,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
getSkillByName = async (req, res) => {
  try {
    const skill = await Skill.find({ description: req.query.description });
    res.status(200).json({
      message: "skill",
      data: skill,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
updateSkill = async (req, res) => {
  try {
    await Skill.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({
      message: "skill update",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
deleteSkill = async (req, res) => {
  try {
    await Skill.deleteOne({ _id: req.params.id });
    res.status(200).json({
      message: "skill deleted",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message,
    });
  }
};
module.exports = {
  createskill,
  getAllSkills,
  getSkillById,
  getSkillByName,
  updateSkill,
  deleteSkill,
};
