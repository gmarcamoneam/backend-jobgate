const Category = require('../Models/Category')

//les function CRUD
createcategory = async (req,res)=>{
    try {
        const newCategory = Category(req.body)
        await newCategory.save();
        res.status(201).json({
            message: 'category crée',
            data: newCategory,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        })
    }
};
getAllCategories = async (req, res) => {
    try {
        const listCategories = await Category.find({});
        res.status(200).json({
            message: 'list of categories',
            data: listCategories,
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
getCategoryById = async (req, res) => {
    try {
        const category = await Category.findById({ _id: req.params.id });
        console.log(category)
        res.status(200).json({
            message: 'category',
            data: category
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    };
}
getCategoryByName = async (req,res)=>{
    try {
        const category = await Category.find({name: req.query.name});
        res.status(200).json({
            message: 'category',
            data: category
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
updateCategory = async (req,res)=>{
    try {
         await Category.updateOne({_id: req.params.id}, req.body);
         res.status(200).json({
            message: 'category update',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });
    }
}
deleteCategory = async(req,res)=>{
    try {
        await Category.deleteOne({_id: req.params.id})
        res.status(200).json({
            message: 'category deleted',
        });
    } catch (error) {
        res.status(400).json({
            message: error.message,
        });  
    }
}
module.exports = { createcategory, getAllCategories, getCategoryById ,getCategoryByName ,updateCategory ,deleteCategory};
